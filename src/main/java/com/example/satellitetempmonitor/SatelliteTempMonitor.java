
package com.example.satellitetempmonitor;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Requirement:
 * Ingest status telemetry data and create alert messages for the following violation conditions:
 * 1. If for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval.
 * 2. If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.
 * 
 * Input Format:
 * The program is to accept a file as input. The file is a an ASCII text file containing pipe delimited records with the
 * following format:
 * <timestamp>|satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
 * 
 *
 * @author Mark Cavender
 */
public class SatelliteTempMonitor {


    public static void main(String[] args) {
        
        if (args.length !=1) {
            System.out.println("This program requires a data file as the first parameter!");
            return;
        }
        
        File telementryFile = new File(args[0]);
        Scanner scnr = null;
        try {
          scnr = new Scanner(telementryFile);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        int TEMPERATURE = 6;
        int TEMPTIME = 0;
        int RED_HIGH_LIMIT = 2;
        int RED_LOW_LIMIT = 5;
        int SATELLITE_ID = 1;
        int COMPONENT = 7;

        Map<Integer, AlertMonitor> redLowBattery = new HashMap<>();
        Map<Integer, AlertMonitor> redHighBattery = new HashMap<>();
        List<Temperature> alerts = new ArrayList<>();  // where the results will be stored.
        if (scnr == null)
            return;

        while(scnr.hasNextLine()){

            String line = scnr.nextLine();

            String[] vals = line.split("\\|");
            Integer satelliteId = Integer.valueOf(vals[SATELLITE_ID]);
            Double redHighLimit = Double.valueOf(vals[RED_HIGH_LIMIT]);
            Double redLowLimit = Double.valueOf(vals[RED_LOW_LIMIT]);
            Double temperature = Double.valueOf(vals[TEMPERATURE]);
            String component = vals[COMPONENT];
            String tempTime = vals[TEMPTIME];

            if (redLowLimit > temperature) {

                AlertMonitor lowRed = redLowBattery.get(satelliteId);
                if (lowRed == null) {
                    lowRed = new AlertMonitor();
                    redLowBattery.put(satelliteId, lowRed);
                }
                lowRed.add(new Temperature(satelliteId, "RED LOW", component, tempTime, temperature));
                if (lowRed.shouldAlert())
                    alerts.add(lowRed.getTemperatures().get(0));
            }

            if (redHighLimit < temperature) {
                AlertMonitor highRed = redHighBattery.get(satelliteId);
                if (highRed == null) {
                    highRed = new AlertMonitor();
                    redHighBattery.put(satelliteId, highRed);
                }
                highRed.add(new Temperature(satelliteId, "RED HIGH", component, tempTime, temperature));
                if (highRed.shouldAlert())
                    alerts.add(highRed.getTemperatures().get(0));
            }

        }
        
        // Convert to JSON using Jackson
        ObjectMapper mapper = new ObjectMapper();
        String json = new String();
        try {
            json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(alerts);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(json);
    }
}
