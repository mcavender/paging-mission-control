/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.satellitetempmonitor;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * AlertMonitor keeps track of the temperatures. When three temperatures occur that
 * violate a constraint within a five minute interval, should alert will return true.
 * 
 *
 * @author mark
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class AlertMonitor implements Serializable {
    
    @JsonIgnore
    static final long FIVE_MINUTE_MILLISECONDS = 5 * 60 * 1000; // five minutes in milliseconds
    
    List<Temperature> temperatures = new ArrayList<>();
    
    /**
     * Adds a temperature to the temperatures array if all the temperatures are within the
     * last 5 minutes.  Any temperature that is not within the last 5 minutes will be removed
     * from the list of temperatures.
     * 
     * @param temperature 
     */
    public void add(Temperature temperature) {
        
        boolean lessThan5Min = true;
        if (temperature == null)
            return;
        
        int i = 0;
        while (lessThan5Min && i < temperatures.size()) {
            if (temperature.getTimestamp().getTime() - temperatures.get(i).getTimestamp().getTime() > FIVE_MINUTE_MILLISECONDS)
                temperatures.remove(i);
            else
                i++;
        }
        
        temperatures.add(temperature);
    }
    
    
    /**
     * @return the first temperature time that was recorded or null if we don't
     * have three temperatures yet.
     */
    public Temperature getFirstAlertRecord() {
        
        if (temperatures != null & temperatures.size() == 3)
            return temperatures.get(0);
        
        return null;
    }
    
    
    /**
     * 
     * @return true if we have three temperatures within 5 minutes.
     */
    public boolean shouldAlert() {
        
        return temperatures.size() == 3;
    }

    public List<Temperature> getTemperatures() {
        return temperatures;
    }

    public void setTemperatures(List<Temperature> temperatures) {
        this.temperatures = temperatures;
    }
    
    
    
}
