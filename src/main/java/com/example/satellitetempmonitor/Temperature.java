
package com.example.satellitetempmonitor;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Temperature is a status reported by a satellite.
 *
 * @author mark
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class Temperature implements Serializable {

    Integer satelliteId; // A unique ID associated with each satellite

    String severity; // Tells what is wrong with the component.

    String component; // The component reporting he temperature.

    @JsonIgnore
    Double temperature; // In degrees Farenheight

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX")
    Timestamp timestamp;

    public Temperature(Integer satelliteId, String severity, String component, String timestampStr, Double temperature) {

        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component = component;
        this.timestamp = getTimeStamp(timestampStr);
        this.temperature = temperature;
    }

    public final Timestamp getTimeStamp(String timeStr) {

        if (timeStr == null)
            return null;
        
        Timestamp timestamp = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd hh:mm:ss.SSS");
            Date parsedDate = dateFormat.parse(timeStr);
            timestamp = new Timestamp(parsedDate.getTime());
        } catch (ParseException e) {
            System.out.println("Exception :" + e);
            System.exit(0);
        }

        return timestamp;

    }

    public Integer getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(Integer satelliteId) {
        this.satelliteId = satelliteId;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

}
