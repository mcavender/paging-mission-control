/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.example.satellitetempmonitor;

import java.sql.Timestamp;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author mark
 */
public class TemperatureTest {
    
    public TemperatureTest() {
    }
    
    /**
     * Test of getTimeStamp method, of class Temperature.
     */
    @Test
    public void testGetTimeStamp() {
        System.out.println("getTimeStamp");
        Temperature instance = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        Timestamp result = instance.getTimestamp();
        assertFalse(result == null);
    }
    
        /**
     * Test of getTimeStamp method, of class Temperature when null is passed as a parameer.
     */
    @Test
    public void testGetTimeStampNull() {
        System.out.println("getTimeStamp");
        String timeStr = "";
        Temperature instance = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        Timestamp result = instance.getTimeStamp(null);
        assertTrue(result == null);
    }

    /**
     * Test of getSatelliteId method, of class Temperature.
     */
    @Test
    public void testGetSatelliteId() {
        System.out.println("getSatelliteId");
        Temperature instance = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        Integer expResult = 1000;
        Integer result = instance.getSatelliteId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setSatelliteId method, of class Temperature.
     */
    @Test
    public void testSetSatelliteId() {
        System.out.println("setSatelliteId");
        Integer satelliteId = 2000;
        Temperature instance = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        instance.setSatelliteId(satelliteId);
        assertEquals(satelliteId, instance.getSatelliteId());
    }

    /**
     * Test of getComponent method, of class Temperature.
     */
    @Test
    public void testGetComponent() {
        System.out.println("getComponent");
        Temperature instance = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        String expResult = "TSAT";
        assertEquals(expResult, instance.getComponent());
    }

    /**
     * Test of setComponent method, of class Temperature.
     */
    @Test
    public void testSetComponent() {
        System.out.println("setComponent");
        String component = "BATT";
        Temperature instance = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        instance.setComponent(component);
        assertEquals(component, instance.getComponent());
    }

    /**
     * Test of getSeverity method, of class Temperature.
     */
    @Test
    public void testGetSeverity() {
        System.out.println("getSeverity");
        Temperature instance = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        String expResult = "RED LOW";
        assertEquals(expResult, instance.getSeverity());
    }

    /**
     * Test of setSeverity method, of class Temperature.
     */
    @Test
    public void testSetSeverity() {
        System.out.println("setSeverity");
        String severity = "YELLOW MEDIUM";
        Temperature instance = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        instance.setSeverity(severity);
        assertEquals(severity, instance.getSeverity());
    }

    /**
     * Test of getTimestamp method, of class Temperature.
     */
    @Test
    public void testGetTimestamp() {
        System.out.println("getTimestamp");
        Temperature instance = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        Timestamp result = instance.getTimestamp();
        assertFalse(result==null);
    }

    /**
     * Test of setTimestamp method, of class Temperature.
     */
    @Test
    public void testSetTimestamp() {
        System.out.println("setTimestamp");
        
        Temperature instance = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        Timestamp timestamp = instance.getTimeStamp("20180101 23:01:06.001");
        instance.setTimestamp(timestamp);
        assertTrue(timestamp instanceof Timestamp);
    }

    /**
     * Test of getTemperature method, of class Temperature.
     */
    @Test
    public void testGetTemperature() {
        System.out.println("getTemperature");
        Temperature instance = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        Double expResult = 97.3;
        Double result = instance.getTemperature();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTemperature method, of class Temperature.
     */
    @Test
    public void testSetTemperature() {
        System.out.println("setTemperature");
        Double temperature = 100.0;
        Temperature instance = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        instance.setTemperature(temperature);
        assertEquals(temperature, instance.getTemperature());
    }
    
}
