/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.example.satellitetempmonitor;

import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author mark
 */
public class AlertMonitorTest {

    public AlertMonitorTest() {
    }

    /**
     * Test of add method, of class AlertMonitor.
     */
    @org.junit.jupiter.api.Test
    public void testAdd() {
        System.out.println("add");
        Temperature temperature = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        AlertMonitor instance = new AlertMonitor();
        instance.add(temperature);
        assertEquals(instance.getTemperatures().size(), 1);
    }

    @org.junit.jupiter.api.Test
    public void testAddTemperatureNull() {
        System.out.println("add Null");
        Temperature temperature = null;
        AlertMonitor instance = new AlertMonitor();
        instance.add(temperature);
        assertEquals(instance.getTemperatures().size(), 0);
    }
    
        /**
     * Test of getFirstAlertRecord method, of class AlertMonitor.
     */
    @org.junit.jupiter.api.Test
    public void testGetFirstAlertRecord() {
        System.out.println("getFirstAlertRecord");
        AlertMonitor instance = new AlertMonitor();
        Temperature temperature1 = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        instance.add(temperature1);
        Temperature temperature2 = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 98.3);
        instance.add(temperature2);
        Temperature temperature3 = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 99.3);
        instance.add(temperature3);
        Temperature expResult = temperature1;
        Temperature result = instance.getFirstAlertRecord();
        assertEquals(expResult, result);
    }

    /**
     * Test the add to make sure that time > 5 minutes are deleted from the list. For this test, the difference between the 2nd
     * and 3rd value are greater than 5 minutes.
     */
    @org.junit.jupiter.api.Test
    public void testAddGreaterThan5Minutes() {
        System.out.println("testAddGreaterThan5Minutes");
        AlertMonitor instance = new AlertMonitor();
        Temperature temperature1 = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        instance.add(temperature1);
        Temperature temperature2 = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 98.3);
        instance.add(temperature2);
        Temperature temperature3 = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:07:05.001", 99.3);
        instance.add(temperature3);

        assertEquals(1, instance.getTemperatures().size());
    }
    
        /**
     * Test the add to make sure that time > 5 minutes are deleted from the list. This time the time between the first and
     * second temperature is greater than 5 minutes.
     */
    @org.junit.jupiter.api.Test
    public void testAddGreaterThan5Minutes2() {
        System.out.println("testAddGreaterThan5Minutes2");
        AlertMonitor instance = new AlertMonitor();
        Temperature temperature1 = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        instance.add(temperature1);
        Temperature temperature2 = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:07:05.001", 98.3);
        instance.add(temperature2);
        Temperature temperature3 = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:08:05.001", 99.3);
        instance.add(temperature3);

        assertEquals(2, instance.getTemperatures().size());
    }

    /**
     * Test of shouldAlert method, of class AlertMonitor without there being any
     * temperatures.
     */
    @org.junit.jupiter.api.Test
    public void testGetFirstAlertRecordWithNoTemperatures() {
        System.out.println("getFirstAlertRecordWithNoTemperatures");
        AlertMonitor instance = new AlertMonitor();
        Temperature expResult = null;
        Temperature result = instance.getFirstAlertRecord();
        assertEquals(expResult, result);
    }

    /**
     * Test of shouldAlert method, of class AlertMonitor.
     */
    @org.junit.jupiter.api.Test
    public void testShouldAlert() {
        System.out.println("shouldAlert");

        AlertMonitor instance = new AlertMonitor();
        Temperature temperature1 = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        instance.add(temperature1);
        Temperature temperature2 = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 98.3);
        instance.add(temperature2);
        Temperature temperature3 = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 99.3);
        instance.add(temperature3);

        assertEquals(true, instance.shouldAlert());
    }
    
        @org.junit.jupiter.api.Test
    public void testShouldAlertOneEntry() {
        System.out.println("shouldAlertOneEntry");

        AlertMonitor instance = new AlertMonitor();
        Temperature temperature1 = new Temperature(1000, "RED LOW", "TSAT", "20180101 23:01:05.001", 97.3);
        instance.add(temperature1);

        assertEquals(false, instance.shouldAlert());
    }
   

    /**
     * Test of getTemperatures method, of class AlertMonitor.
     */
    @org.junit.jupiter.api.Test
    public void testGetTemperatures() {
        System.out.println("getTemperatures");
        AlertMonitor instance = new AlertMonitor();
        List<Temperature> result = instance.getTemperatures();
        assertEquals(0, result.size());
    }

    /**
     * Test of setTemperatures method, of class AlertMonitor.
     */
    @org.junit.jupiter.api.Test
    public void testSetTemperatures() {
        System.out.println("setTemperatures");
        List<Temperature> temperatures = new ArrayList<>();
        AlertMonitor instance = new AlertMonitor();
        instance.setTemperatures(temperatures);
        assertEquals(0, instance.getTemperatures().size());

    }

}
